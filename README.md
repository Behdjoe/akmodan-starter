# Akmodan Starter
> Akmodan starter project. Designed to get you up and running fast, easy & pain free. Akmodan comes pre-configured & pre equipped to deliver production ready front-end at your fingertips. You'll have everything you need and nothing you don't.
>
> Demo: https://akmodan-starter.netlify.com


## Requirements
- Nodejs (https://nodejs.org)

## Features
- Minimalistic
- Fast & light, only ~71KB Gzipped
- Write ES6, compile to vanilla Javascript
- Powerful templating engine
- Hotkey support
- Built on Nuxts + VueJs
- Write .vue component files
- Pages + easy routing
- Global state management
- Use URL query parameters
- Clean URL's (/about/more)
- Transition animations
- Styling (SCSS) + Utils + Vars
- Axios for ajax calls
- Custom Fonts
- Static site builder
- Fancy error pages (404, 500 ...)
- & more


## Build Setup

``` bash
# Install dependencies
$ npm install

# Serve with hot reload at localhost:3000
$ npm run dev

# Build for production and launch server
$ npm run build && npm start

# Generate static project (SSR)
# this is recommended for production deployments as it generates a static website.
$ npm run generate
```

## Browser Support 
- All modern browsers
- IE 10 >
- MS Edge

## Documentation
- [Vue](https://vuejs.org/)
- [Nuxt](https://nuxtjs.org/)

## Screenshot
![Akmodan starter](https://i.imgur.com/bFl3esw.png)

