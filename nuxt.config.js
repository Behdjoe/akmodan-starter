const app = {
  mode: 'spa', // just leave it like this, trust me.
  siteTitle: 'Akmodan Starter 🚀',
  pageDescription: 'Akmodan inc.',
  fonts: '/fonts/fonts-all.css',
  themeColor: '#09c',
  loadingIndicator: 'folding-cube',
  favicon: '/img/favicon.ico',
  css: ['~/assets/scss/all.scss'],
  styleResources: {
    // scss variables & utils injection into .vue components
    scss: ['src/assets/scss/utils/_all.scss', 'src/assets/scss/vars/_all.scss']
  },
  baseUrl: () => {
    let protocol = process.env.PRODUCTION ? 'https://' : 'http://'
    // get host and port from package.json under 'config'
    return protocol + process.env.npm_package_config_nuxt_host + ':' + process.env.npm_package_config_nuxt_port + process.env.npm_package_config_nuxt_path
  },
  srcDir: 'src',
  distFolder: 'dist',
  // middleware: ['check-auth'],
  modules: [
    '@nuxtjs/axios', // for universal ajax calls.
    '@nuxtjs/sitemap' // auto sitemap
  ],
  plugins: [
    { src: '~/plugins/v-click-outside.js', ssr: false }, // click outside plugin (server side rendered set to 'false')
    { src: '~/plugins/v-focus.js', ssr: false } // focus input on load
  ],
  script: [
    // examples (static folder):
    // { src: '/js/analytics.js', body: true }
  ],
  vendor: [], // use in conjunction with 'universal' mode.
  // routes () {
  //   // TODO: fix hardcoded news categories
  //   const newsPages = ['reviews', 'technology', 'win']
  //   const payload = {
  //     info: true
  //   }
  //   // return '/news/' + list
  //   return newsPages.map(item => {
  //     return {
  //       route: '/news/' + item,
  //       // TODO: integrate payload mechanism correctly
  //       payload: payload
  //     }
  //   })
  // }
}

module.exports = {
  mode: app.mode,
  dev: (process.env.NODE_ENV !== 'production'),
  env: {
    title: app.pageDescription,
    author: process.env.npm_package_author_name
  },
  srcDir: app.srcDir,
  // HTML head
  head: {
    title: app.siteTitle,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0' },
      { hid: 'description', name: 'description', content: app.pageDescription }
    ],
    script: app.script,
    noscript: [
      { innerHTML: process.env.npm_package_description, body: true }
    ],
    // static content goes here
    link: [
      { rel: 'icon', type: 'image/x-icon', href: app.favicon },
      { rel: 'stylesheet', href: app.fonts }
    ]
  },
  transition: 'page',
  css: app.css,
  loadingIndicator: {
    // backgroubnd: 'white',
    name: app.loadingIndicator,
    color: app.themeColor
  },
  loading: {
    color: app.themeColor,
    height: '2px',
  },
  router: {
    // Redirect 404's to index page
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'redirect',
        path: '*',
        component: resolve(__dirname, 'src/pages/index.vue')
      })
    },
    middleware: app.middleware
  },
  //axios config
  axios: {
    BaseURL: app.baseUrl(),
    browserBaseURL: process.env.npm_package_config_nuxt_path,
  },
  modules: app.modules,
  plugins: app.plugins,
  build: {
    // for CDN support
    // publicPath: 'https://cdn.domain.org',
    vendor: app.vendor,
    styleResources: {
      scss: app.styleResources.scss
    },
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        })
      }
    }
  },
  generate: {
    dir: app.distFolder,
    interval: 1000,
    minify: {
      removeComments: true,
      sortClassName: false
    },
    routes: app.routes,
    subFolders: false //flat folders
  },
  sitemap: {
    path: '/sitemap.xml',
    hostname: app.baseUrl(),
    cacheTime: 1000 * 60 * 60,
    generate: true,
    exclude: [
      '/users',
      '/users/**',
      '/auth/**'
    ]
  }
}
