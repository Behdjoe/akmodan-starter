export const navigation = {
  namespaced: true,
  state: {
    feed: [
      {
        label: 'home',
        url: '/'
      },
      {
        label: 'about',
        url: '/about'
      }
    ]
  },
  getters: {
    GET_FEED(state) {
      return state.feed
    }
  },
  mutators: {
    NEW_ITEM(state, payload) {
      console.warn(state, payload)
    }
  }
}
