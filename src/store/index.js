import Vuex from 'vuex'
import { navigation } from './navigation'
export default () => {
  return new Vuex.Store({
    modules: {
      NAVIGATION: navigation
    }
  })
}
