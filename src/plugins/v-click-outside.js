import Vue from 'vue'
import vClickOutside from 'v-click-outside'
// usage: v-click-outside="doFunction"
Vue.use(vClickOutside)
