module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    "eslint:recommended",
    "plugin:vue/recommended",
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  // add custom rules here
  rules: {
    "semi": [2, "never"],
    "no-console": "off",
    "vue/max-attributes-per-line": "off",
    "vue/mustache-interpolation-spacing": "off",
    "vue/order-in-components": "off",
    "vue/attributes-order": "off"
  }
}
